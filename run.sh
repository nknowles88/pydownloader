#!/bin/bash
# vim: et ts=2 sw=2 sts=2

set -e

function bootstrap {
  local py_ver=$1
  podman build --pull --no-cache -t $py_ver -f docker/$py_ver.Dockerfile
  podman run -it --rm -v $PWD:/pypi:Z $py_ver /pypi/run.sh $py_ver
  podman rmi $py_ver
}

if [ -z "$container" ]; then
  # Not in a container
  mkdir -p pypi

  # Cache existing pacakges so we can compute deltas later
  declare -A old
  if [ ! -f old.txt ]; then
    find pypi -type f | sort > old.txt
  fi
  while read line; do
    old["$line"]=1
  done < old.txt

  # Download pkgs (todo parse args?)
  bootstrap py27
  #bootstrap py36
  bootstrap py38
  bootstrap py39
  bootstrap py310
  podman image prune -f

  # Compute deltas
  rm -rf deltas
  mkdir deltas
  for x in $(find pypi -type f | sort); do
    if [ ! ${old["$x"]} ]; then
      cp "$x" deltas/
      echo "New package: $x"
    fi
  done
else
  # In container
  cd /pypi
  # Pip doesn't like downloading this multiple times
  rm -f pypi/vsphere-automation-sdk-*.zip
  rm -f pypi/capnp-stub-generator-*.zip

  # x86_64
  reqs="reqs-$1.txt"
  [ ! -f "$reqs" ] && reqs=reqs.txt
  while read line; do
    [[ -z "$line" || ${line:0:1} = '#' ]] && continue
    # Download sdist for numpy only for py36. There is an issue packaging a
    # virtualenv in an RPM using the wheel.
    if [[ "$line" = numpy && "$1" = py36 ]]; then
      pip download -d pypi --no-binary :all: numpy
      continue
    fi
    echo -e "\n========================= $line ($1 | x86_64)"
    pip download -d pypi $line
  done < "$reqs"

  # AArch64
  if [ "$1" != py27 ]; then
    reqs="reqs-$1.aarch64.txt"
    [ ! -f "$reqs" ] && reqs=reqs.aarch64.txt
    while read line; do
      echo -e "\n========================= $line ($1 | aarch64)"
      pip download -d pypi --platform manylinux2014_aarch64 --only-binary :all: $line
    done < "$reqs"
    echo
  fi
fi

exit 0
