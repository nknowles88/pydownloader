FROM python:2.7

RUN python -m pip --version
RUN python -m pip --no-cache install --user -U pip
RUN python -m pip --version

CMD ["/bin/bash"]
