FROM python:3.9

RUN python3 -m pip --version
RUN python3 -m pip --no-cache install --user -U pip
RUN python3 -m pip --version

CMD ["/bin/bash"]
